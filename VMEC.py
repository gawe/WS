# -*- coding: utf-8 -*-
"""
Created on Thu Nov 30 11:22:01 2017

@author: tcw
"""

from osa import Client
import os
import numpy as np
from pyversion.version import urllib
from urllib.request import urlretrieve
import matplotlib.pyplot as plt


VClient = Client("http://esb:8280/services/vmec_v5?wsdl")

def create_local_configs(vid, vmec_local=os.environ['CONFBC']):
    ''' creates local copy of boozer files for TRAVIS'''
    url='http://svvmec1.ipp-hgw.mpg.de:8080/vmecrest/v1/'
    opath=os.path.join(vmec_local,  vid + '_wout.txt')
    try:
        urlretrieve(url+vid+'/wout.txt',opath)
    except:
        try:
            url='http://svvmec1.ipp-hgw.mpg.de:8080/vmecrest/v1/run/'
            urlretrieve(url+vid+'/wout.txt',opath)
        except:
            raise ValueError('could not retrieve '+url+vid+'/wout.txt')

    # url='http://svvmec1.ipp-hgw.mpg.de:8080/vmecrest/v1/geiger/w7x/'

    # if not os.path.exists(vmec_local):
    #     os.mkdir(vmec_local)
    # label = {}
    # stream = {}
    # label[0]  = 'DBM000'
    # stream[0] = '1000_1000_1000_1000_+0750_+0750/01/00/boozer.txt'
    # label[1]  = 'EIM000'
    # stream[1] = '1000_1000_1000_1000_+0000_+0000/01/00/boozer.txt'
    # label[2]  = 'EIM065'
    # stream[2] = '1000_1000_1000_1000_+0000_+0000/01/04m/boozer.txt'
    # label[3]  = 'EIM200'
    # stream[3] = '1000_1000_1000_1000_+0000_+0000/01/12m/boozer.txt'
    # label[4]  = 'KJM000'
    # stream[4] = '1020_1080_0930_0843_+0000_+0000/01/00/boozer.txt'
    # label[5]  = 'KJM065'
    # stream[5] = '0972_0926_0880_0852_+0000_+0000/01/00jh/boozer.txt'
    # label[6]  = 'FTM000'
    # stream[6] = '1000_1000_1000_1000_-0690_-0690/01/00/boozer.txt'
    # label[7]  = 'EEM000'
    # stream[7] = '1000_1000_1000_1000_+0390_+0390/05/0000/boozer.txt'

    # for i in range(len(label.items())):
    #     urllib.urlretrieve(url+stream[i],vmec_local+label[i]+'_boozer.bc')


def create_local_boozer_configs(vid, vmec_local=os.environ['CONFBC']):
    ''' creates local copy of boozer files for TRAVIS'''
    url='http://svvmec1.ipp-hgw.mpg.de:8080/vmecrest/v1/'
    opath=os.path.join(vmec_local, vid + '_boozer.txt')
    try:
        urlretrieve(url+vid+'/boozer.txt',opath)
    except:
        raise ValueError('could not retrieve '+url+vid+'/boozer.txt')


def create_local_dkes_configs(vid, dkfilename):
    ''' creates local copy of DKES and Boozer for neo_ntss'''
    url='http://svvmec1.ipp-hgw.mpg.de:8080/vmecrest/v1/'

    dkes_dir =  os.getenv('DKHOME')
    bc_dir =  os.getenv('CONFBC')

    file = os.path.splitext(dkfilename)[0]

    print(url+vid+'/' + dkfilename )

    if (os.path.isfile(os.path.join(dkes_dir, file + '.dk'))) and (os.path.isfile(os.path.join(bc_dir, file + '.bc'))):
        return

    else:
        #get dkes
        try:
            urlretrieve(url+vid+'/' + dkfilename, os.path.join(dkes_dir, file + '.dk'))
        except:
            raise ValueError('could not retrieve dkes file '+url+vid+'/' + file)

        #get boozer
        try:
            urlretrieve(url+vid+'/boozer.txt', os.path.join(bc_dir, file + '.bc'))
        except:
            raise ValueError('could not retrieve boozer file '+url+vid+'/boozer.txt')





def get_config_url(label,full_path=0):
    ''' returns link to VMEC url

        usage: Vid=get_config_url(label)

        availabel labels:
            DBM000
            EIM000
            EIM065
            EIM200
            KJM000
            KJM065
            FTM000
            EEM000

                    '''
    co={'DBM000': '/w7x/1000_1000_1000_1000_+0750_+0750/01/00',
#        'EJM000': '/w7x/1000_1000_1000_1000_+0000_+0000/01/08m', # 1.3% beta
#        'EJM000': '/w7x/1000_1000_1000_1000_+0000_+0000/01/16m', # 2.7% beta
        'EJM000': '/w7x/1000_1000_1000_1000_+0000_+0000/01/00jh_l',
        'EIM000': '/w7x/1000_1000_1000_1000_+0000_+0000/01/00',
        'EIM065': '/w7x/1000_1000_1000_1000_+0000_+0000/01/04m',
        'EIM200': '/w7x/1000_1000_1000_1000_+0000_+0000/01/12m',
        'KKM000': '/w7x/1020_1080_0930_0843_+0000_+0000/01/00',
        'KJM000': '/w7x/1020_1080_0930_0843_+0000_+0000/01/00',
        'KJM065': '/w7x/0972_0926_0880_0852_+0000_+0000/01/00jh',
        'FTM000': '/w7x/1000_1000_1000_1000_-0690_-0690/01/00',
        'EEM000': '/w7x/1000_1000_1000_1000_+0390_+0390/05/0000',
       }

    if full_path:
        return 'http://svvmec1.ipp-hgw.mpg.de:8080/vmecrest/v1/geiger' + co[label]
    else:
        return co[label]


def get_basic_id(conf):
    vid={
        'EIM': 'w7x_ref_3',
        'EJM': 'w7x_ref_3',
        'AIM': 'w7x_ref_21',
        'KJM': 'w7x_ref_27',
        'KKM': 'w7x_ref_27',
        'FTM': 'w7x_ref_15',
        'DBM': 'w7x_ref_18',
        'ILM': 'w7x_ref_40',
        'ILD': 'w7x_ref_40' }
    return vid[conf]


def get_coil_currents(Vid):
    ''' Ic = get_CoilCurrents(VMEC_ID)'''
    IC = np.zeros(14)  #include also trim + cc as used in FLT
    IC[:7] = VClient.service.getCoilCurrents(Vid)
    return IC


def get_config_sid(Bconf):
    co={'DBM000': 'w7x_ref_18',
#        'EJM000': 'w7x_ref_8', # 1.3% beta
#        'EJM000': 'w7x_ref_10', # 2.7% beta
#        'EJM000': 'w7x_ref_13', # 2.7% beta
        'EIM000': 'w7x_ref_1', # w7x_ref_1
        'EIM065': 'w7x_ref_7',
        'EIM200': 'w7x_ref_9',
        'KJM000': 'w7x_ref_163',
        'KKM000': 'w7x_ref_163',
        'KJM065': 'w7x_ref_27',
        'FTM000': 'w7x_ref_15',
        'EEM000': 'w7x_ref_82',
       }
    return co[Bconf]

def get_config_amin(Bconf):
    co={'DBM000': 0.54,
#        'EJM000': 'w7x_ref_8', # 1.3% beta
#        'EJM000': 'w7x_ref_10', # 2.7% beta
        'EJM000': 0.51, # 2.7% beta
        'EIM000': 0.51, # w7x_ref_1
#        'EIM065': 'w7x_ref_7',
#        'EIM200': 'w7x_ref_9',
        'KJM000': 0.50,
        'KKM000': 0.50,
#        'KJM065': 'w7x_ref_27',
        'FTM000': 0.50,
        'EEM000': 0.49,
       }
    return co[Bconf]

# def get_reff(x,y,z,Vid):
#     ''' reff = get_reff(x,y,z,VMEC_ID)'''
#     p3d = VClient.types.Points3D()
#     p3d.x1 = x
#     p3d.x2 = y
#     p3d.x3 = z
#     B = VClient.service.magneticField(Vid,p3d)
#     reff=VClient.service.getReff(Vid,p3d)

def get_reff(Vid,x,y,z):
    ''' reff = get_reff(x,y,z,VMEC_ID)'''
    p3d = VClient.types.Points3D()
    p3d.x1 = x
    p3d.x2 = y
    p3d.x3 = z
    return VClient.service.getReff(Vid,p3d)


def get_B_at_axis(Vid,phi=0.):
    tmp=VClient.service.getMagneticAxis(Vid, phi*np.pi/180.)
    s = VClient.service.magneticField(Vid,tmp)

    return np.sqrt(np.square(s.x1) + np.square(s.x2) + np.square(s.x3))[0]


def get_axis(Vid,phi=0.):
    tmp=VClient.service.getMagneticAxis(Vid, phi*np.pi/180.)
    return tmp.x1[0]


def get_iota_profile(Vid):
    iota=VClient.service.getIotaProfile(Vid)
    s=np.linspace(0,1,len(iota))
    r=np.sqrt(s)
    return r, iota


def get_B(Vid,x,y,z,B00=None,norm=1):
    '''B = get_B(VMECid,x,y,z)'''
    if B00 is not None:
        B00_unscaled=get_B_at_axis(Vid)
        scale=B00/B00_unscaled
    else:
        scale=1.
    p3d = VClient.types.Points3D()
    p3d.x1 = x
    p3d.x2 = y
    p3d.x3 = z
    s = VClient.service.magneticField(Vid,p3d)


    Bx=np.asarray(s.x1)*scale
    By=np.asarray(s.x2)*scale
    Bz=np.asarray(s.x3)*scale
    Babs=np.sqrt(np.square(Bx) + np.square(By) + np.square(Bz))
    if norm==0:
        #return {'Babs':Babs, 'Bx':Bx, 'By':By, 'Bz':Bz}
        return np.array([Bx[0], By[0], Bz[0]]).T
    else:
        return Babs


# def get_B(x,y,z,Vid):
#     '''B = get_B(x,y,z,VMEC_ID)'''
#     p3d = VClient.types.Points3D()
#     p3d.x1 = x
#     p3d.x2 = y
#     p3d.x3 = z
#     s = VClient.service.magneticField(Vid,p3d)
#     Bx=s.x1
#     By=s.x2
#     Bz=s.x3
#     Babs=np.sqrt(np.square(Bx) + np.square(By) + np.square(Bz))
#     return {'Babs':Babs, 'Bx':Bx, 'By':By, 'Bz':Bz}

def get_minor_radius(Vid):
    '''a = get_minor_radius(VMEC_ID)'''
    tmp=VClient.service.getReffProfile(Vid)
    return tmp[len(tmp)-1]


def get_central_pressure(Vid):
    return VClient.service.getPressureProfile(Vid)[0]


def fluxsurfaces(s,phi,Vid,N=256,disp=1,_ax=plt, fmt='k-'):
    '''
    fluxsurfaces(s,phi,Vid,N=256,disp=0)
    '''

    if s is None:
        s=np.linspace(0.05,1,10)

    if phi is None:
        # print('using pgideg=0')
        phi=0

    if isinstance(s,int):
        nn=1
    else:
        nn=len(s)
        phi=np.ones(nn)*phi

    w=VClient.service.getFluxSurfaces(Vid, phi*np.pi/180.0, s, N)
    R=np.zeros((nn,N))
    z=np.zeros((nn,N))
    for ii in range(nn):
        if isinstance(w[ii].x1, list) and isinstance(w[ii].x2, list):
            R1=np.sqrt(np.square(w[ii].x1) + np.square(w[ii].x2))
            z1=w[ii].x3
        else:
            R1=w[ii].x1
            z1=w[ii].x3

        if disp:
            _ax.plot(R1,z1,fmt)
        R[ii]=R1
        z[ii]=z1

    if disp:
        _ax.axis('equal')

    return R, z


def mag2xyz(Vid,s,theta,phi):
    '''xyz = mag2xyz(VMEC_ID,s,theta,phideg)'''
    vmec_coords = VClient.types.Points3D()
    vmec_coords.x1 = s
    vmec_coords.x2 = theta*np.pi/180.
    vmec_coords.x3 = phi*np.pi/180.
    cyl = VClient.service.toCylinderCoordinates(Vid, vmec_coords)
    x=cyl.x1*np.cos(cyl.x2)
    y=cyl.x1*np.sin(cyl.x2)
    z=cyl.x3

    return x,y,z



def get_VMEC_ID_from_coilcurrents(conf):

    I=get_VMEC_ID_from_coilcurrents(conf)
    import requests
    correction=-200.
    url1='http://esb.ipp-hgw.mpg.de:8280/services/encode_w7x_config_extra?'
    url2='i1=%1.2f&i2=%1.2f&i3=%1.2f&i4=%1.2f&i5=%1.2f&ia=%1.2f&ib=%1.2f&b0_req=2.5&correction=%1.2f'%(I[0],I[1],I[2],I[3],I[4],I[5],I[6],correction)
    res = requests.get(url1 + url2)
    if res.status_code == 200:
        r = res.json()
        return r
